<?php include "header.php"; ?>
    <main class="main">
        <section class="our-cruise-1">
            <div class="container-fuild">
                <div class="content">
                    <div class="bg-video">
                        <div class="container">
                            <div class="list-link">
                                <a href="#" class="list-link-item">
                                    <figure>
                                        <img src="./dist/images/Home.svg" alt="svg">
                                    </figure>
                                    <span>Home</span>
                                </a>
                                <span class="untitled">/</span>
                                <a href="#" class="list-link-item">
                                    <span>Cruise Itineraries</span>
                                </a>
                                <span class="untitled">/</span>
                                <a href="#" class="list-link-item active">
                                    <span>2 Days 1 Night</span>
                                </a>
                                <span class="untitled">/</span>
                            </div>
                        </div>
                        <!--                    <video autoplay muted loop id="video">-->
                        <!--                        <source src="dist/images/video.mp4" type="video/mp4">-->
                        <!--                    </video>-->
                        <img src="./dist/images/f9b8e291ccd2542498cc92bfca92791f.jpg" alt="">
                    </div>
                    <div class="content-main">
                        <div class="title">
                            <h2>Cruise Itineraries</h2>
                        </div>
                        <div class="text">
                            <span>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</span>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section class="section about-section-6 wedding-2">
            <div class="container">
                <div class="row justify-content-between unique">
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="img">
                            <div class="img-big">
                                <picture>
                                    <img src="./dist/images/0d5230c9aa8a2cecdc6d316db32662e8.jpg" alt="">
                                </picture>
                            </div>
                            <div class="img-mini">
                                <picture>
                                    <img src="./dist/images/0d5230c9aa8a2cecdc6d316db32662e8.jpg" alt="">
                                </picture>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-md-6 col-12">
                        <div class="content-unique">
                            <div class="title">
                                <h3>Internation Luxury Weddings Cruise in Vietnam</h3>
                            </div>
                            <hr>
                            <div class="content">
                                <p>
                                    Aside from our experience and luxury, Indochina Sails has famously been known for
                                    the highly individual excursions offered on our cruises. We fill our roles as
                                    innovators by offering fresh ways to absorb the majesty of Halong and Lan Ha bays.
                                </p>
                                <p>
                                    One such way in which we do this is with our ‘floating bar’, a truly magical
                                    candlelit dinner in a cave, bookable via special appointment with our staff.
                                </p>
                                <p>
                                    The Indochine Cruise is also the first cruise to offer multiple dining rooms for the
                                    freedom of our guests. Our restaurants will provide different types of food
                                    presented in different styles, meaning that passengers get to choose what they are
                                    feeling between buffet, set menu and a la carte meals.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-section-3 section">
            <div class="container">
                <div class="div-content">
                    <div class="row dis-our align-items-center">
                        <div class="col-xl-6 col-md-7 col-sm-12">
                            <div class="content-our">
                                <div class="title">
                                    <h3>Personalized Experience</h3>
                                </div>
                                <hr>
                                <div class="content">
                                    <p>
                                        Indochina Sails is one of the first companies specializing in luxury cruises in
                                        Ha
                                        long Bay with more than 20 years of significant experience. Its 5 new ships
                                        named
                                        Indochina Sails and Valentine recently came into service in 2007, 2008 and 2010
                                        confirmed again its objectives of charm, comfort and perfect services; increased
                                        its
                                        capacity many times higher before. Indochina Sails became the first choice of
                                        many
                                        tourists from inside and outside Vietnam for their trip to the World Natural
                                        Heritage – Ha Long bay.
                                    </p>
                                    <p>
                                        From 2019 guests will be offered access to a new land experiencing its dawn in
                                        tourism. The Indochine Cruise is the newest cruise in Lan Ha Bay, a seascape
                                        full of
                                        Halong’s famous limestone spires, rugged caves and golden sands, but with far
                                        fewer
                                        tourist boats and much more in the ways of space and calm atmosphere.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 col-sm-12">
                            <div class="image-our">
                                <picture>
                                    <img src="./dist/images/b61e964f00c69e538d1d0682fe52d93e.jpg" alt="">
                                </picture>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="cruise-section-1 wedding-4 section section-last ">
            <div class="bg-yle section section-last">
                <div class="container">

                    <div class="list-event ">
                        <div class="row row-10">
                            <div class="col-md-6 col-lg-4 col-12 p-10 col-item">
                                <div class="box">
                                    <a href="#">
                                        <div class="picture">
                                            <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                        </div>
                                        <div class="desc">
                                            <h3>Indochine Premium Ha Long</h3>
                                            <span class="line"></span>
                                            <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 col-12 p-10 col-item">
                                <div class="box">
                                    <a href="#">
                                        <div class="picture">
                                            <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                        </div>
                                        <div class="desc">
                                            <h3>Indochine Premium Ha Long</h3>
                                            <span class="line"></span>
                                            <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-md-6 col-lg-4 p-10 col-item">
                                <div class="box">
                                    <a href="#">
                                        <div class="picture">
                                            <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                        </div>
                                        <div class="desc">
                                            <h3>Indochine Premium Ha Long</h3>
                                            <span class="line"></span>
                                            <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </main>
<?php include "footer.php"; ?>