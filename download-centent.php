<?php include "header.php"; ?>
    <main class="main">
        <section class="section breadcrumbs section-top">
            <div class="container">
                <div class="bread">
                    <h5><img src="./dist/images/Home2.png" alt="">Home</h5> / <span>Download Center</span>
                </div>
            </div>
        </section>
        <section class=" download-section-1 section-last">
            <div class="container">
                <div class="feedback-1">
                    <div class="big-feedback">
                        <div class="img">
                            <img src="./dist/images/0fa1e3d992973eb5082b5075a3451136.jpg" alt="">
                        </div>
                        <div class="content">
                            <div class="icon">
                                <img src="./dist/images/phay.svg" alt="">
                            </div>
                            <div class="content-item">
                                <p>Dear travel agents,</p>
                                <p>

                                    Welcome to Indochina Sails’ Download Center, designed specifically to cater to
                                    professionals in the travel industry, all the information you need to make reservations.
                                    We value your business and offer all the necessary resources to ensure that you and your
                                    clients receive excellent rates and an unforgettable stay at Indochina Sails. You can
                                    feel assured each distinct cruise will give your clients a true World Natural Heritage
                                    experience by booking them the perfect vacation at Indochina Sails. We welcome
                                    relationships with travel professionals. Please click to view our sales kit. Direct any
                                    questions to info@indochinasails.com or +84 982 042 426
                                </p>
                            </div>

                        </div>
                    </div>
                    <div class="gr-feedback">
                        <div class="gr-feedback-item">
                            <a href="">
                                <div class="img">
                                    <picture>
                                        <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                                    </picture>
                                </div>
                                <div class="content-title">
                                    <h4>Indochine Sails - Ha Long Bay</h4>
                                </div>
                            </a>
                        </div>
                        <div class="gr-feedback-item">
                            <a href="">
                                <div class="img">
                                    <picture>
                                        <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                                    </picture>
                                </div>
                                <div class="content-title">
                                    <h4>Indochine Sails - Ha Long Bay</h4>
                                </div>
                            </a>
                        </div>
                        <div class="gr-feedback-item">
                            <a href="">
                                <div class="img">
                                    <picture>
                                        <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                                    </picture>
                                </div>
                                <div class="content-title">
                                    <h4>Indochine Sails - Ha Long Bay</h4>
                                </div>
                            </a>
                        </div>
                        <div class="gr-feedback-item">
                            <a href="">
                                <div class="img">
                                    <picture>
                                        <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                                    </picture>
                                </div>
                                <div class="content-title">
                                    <h4>Indochine Sails - Ha Long Bay</h4>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php include "footer.php"; ?>