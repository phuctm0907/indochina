<?php include "header.php"; ?>
<main class="main">
    <section class=" breadcrumbs section-top">
        <div class="container">
            <div class="bread">
                <h5><img src="./dist/images/Home2.png" alt="">Home</h5> / <span>Indochina Sails -  Ha Long Bay</span>
            </div>
        </div>
    </section>
    <section class="folder-section-1">
        <div class="container">
            <div class="note-tab">
                <div class="title">
                    <h3>Folders</h3>
                </div>

            </div>
            <div class="gr-folder">
                <div class="item">
                    <a href="">
                        <div class="img">
                            <picture>
                                <img src="./dist/images/Group%201000006356.png" alt="">
                            </picture>
                            <div class="icon-net">
                                <img src="./dist/images/Subtract.png" alt="">
                            </div>
                        </div>
                        <div class="title">
                            <h4>Sales kit</h4>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="">
                        <div class="img">
                            <picture>
                                <img src="./dist/images/Group%201000006356.png" alt="">
                            </picture>
                            <div class="icon-net">
                                <img src="./dist/images/Subtract.png" alt="">
                            </div>
                        </div>
                        <div class="title">
                            <h4>Sales kit</h4>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="">
                        <div class="img">
                            <picture>
                                <img src="./dist/images/Group%201000006356.png" alt="">
                            </picture>
                            <div class="icon-net">
                                <img src="./dist/images/Subtract.png" alt="">
                            </div>
                        </div>
                        <div class="title">
                            <h4>Valentine Premium</h4>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="">
                        <div class="img">
                            <picture>
                                <img src="./dist/images/Group%201000006356.png" alt="">
                            </picture>
                            <div class="icon-net">
                                <img src="./dist/images/Subtract.png" alt="">
                            </div>
                        </div>
                        <div class="title">
                            <h4>Valentine Premium</h4>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="">
                        <div class="img">
                            <picture>
                                <img src="./dist/images/Group%201000006356.png" alt="">
                            </picture>
                            <div class="icon-net">
                                <img src="./dist/images/Subtract.png" alt="">
                            </div>
                        </div>
                        <div class="title">
                            <h4>Valentine Premium</h4>
                        </div>
                    </a>
                </div>
                <div class="item">
                    <a href="">
                        <div class="img">
                            <picture>
                                <img src="./dist/images/Group%201000006356.png" alt="">
                            </picture>
                            <div class="icon-net">
                                <img src="./dist/images/Subtract.png" alt="">
                            </div>
                        </div>
                        <div class="title">
                            <h4>Valentine Premium</h4>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </section>
</main>
<?php include "footer.php"; ?>