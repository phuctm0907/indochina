(function ($) {
  window.onload = function () {
    $(document).ready(function () {
      sliderPage();
      counterDot();
      custTab();
      animateAOS();
        Placeholder()
    });
  };
})(jQuery);

function sliderPage() {
  if ($(".home-slide-1 .item-slide").length > 1) {
    $(".home-slide-1").flickity({
      pageDots: false,
      prevNextButtons: true,
      contain: true,
      cellAlign: "left",
      imagesLoaded: true,
      draggable: true,
      wrapAround: true,
      autoPlay: true,
    });
  }
  if ($(".home-slide-2 .joy-item").length > 1) {
    $(".home-slide-2").flickity({
      pageDots: false,
      prevNextButtons: true,
      contain: true,
      cellAlign: "left",
      imagesLoaded: true,
      draggable: true,
      wrapAround: true,
      autoPlay: true,
    });
  }
}

function counterDot() {
  var $counter = $(".home-slide-1").flickity();
  var $counterStatus = $(".counter-status");
  var flkty = $counter.data("flickity");

  function updateStatus() {
    var cellNumber = flkty.selectedIndex + 1;
    $counterStatus.text('0' + cellNumber);
  }
  updateStatus();
  $counter.on("change.flickity", updateStatus);
}

function custTab() {
    $(".tab-pane").hide();
    $(".type").each(function() {
        $(this).find('li:first').addClass("active");
        $(this).closest('.type-list').nextAll('.type-content').find('.tab-pane:first').show();
    });
    $(".type li").click(function() {
        var cTab = $(this);
        cTab.siblings('li').removeClass("active");
        cTab.addClass("active");
        cTab.closest('.type-list').nextAll('.type-content:first').find('.tab-pane').hide();
        var activeTab = $(this).attr("rel");
        $(activeTab).fadeIn();
        return false;
    });
}

function animateAOS() {
  AOS.init({
    once: true,
    disable: function () {
      var maxWidth = 991;
      return window.innerWidth < maxWidth;
    },
  });
  AOS.refresh();
}
