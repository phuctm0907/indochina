<?php include "header.php"; ?>
    <main class="main">
        <section class="our-cruise-1">
            <div class="container-fuild">
                <div class="content">
                    <div class="bg-video">
                        <div class="container">
                            <div class="list-link">
                                <a href="#" class="list-link-item">
                                    <figure>
                                        <img src="./dist/images/Home.svg" alt="svg">
                                    </figure>
                                    <span>Home</span>
                                </a>
                                <span class="untitled">/</span>
                                <a href="#" class="list-link-item">
                                    <span>Cruise Itineraries</span>
                                </a>
                                <span class="untitled">/</span>
                                <a href="#" class="list-link-item active">
                                    <span>2 Days 1 Night</span>
                                </a>
                                <span class="untitled">/</span>
                            </div>
                        </div>
                        <!--                    <video autoplay muted loop id="video">-->
                        <!--                        <source src="dist/images/video.mp4" type="video/mp4">-->
                        <!--                    </video>-->
                        <img src="./dist/images/f9b8e291ccd2542498cc92bfca92791f.jpg" alt="">
                    </div>
                    <div class="content-main">
                        <div class="title">
                            <h2>Cruise Itineraries</h2>
                        </div>
                        <div class="text">
                            <span>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</span>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section class="section section-last contact-section-1 ">
            <div class="container">
                <div class="title">
                    <h2>Get in touch with us</h2>
                    <p>Please fill in your details below and we will contact you by phone or email shortly.</p>
                </div>
                <div class="form-contact row">
                    <div class="form col-xl-8 col-lg-7 col-md-6 col-12">
                        <form action="">
                            <div class="gr-note">
                                <div class="note-cols f1">
                                    <div class="item">
                                        <input type="text" class="form-control">
                                        <label for="">Name <span>*</span></label>
                                    </div>
                                    <div class="item">
                                        <input type="email" class="form-control">
                                        <label for="">Email <span>*</span></label>
                                    </div>
                                </div>
                                <div class="note-cols f2">
                                    <div class="item">
                                        <input type="text" class="form-control">
                                        <label for="">Address <span>*</span></label>
                                    </div>
                                    <div class="item phone">
                                        <select name="" id="">
                                            <option value="">+84</option>
                                            <option value="">+83</option>
                                            <option value="">+123</option>
                                            <option value="">+77</option>

                                        </select>
                                        <input type="tel" class="form-control" placeholder="Mobile Phone">

                                    </div>
                                </div>
                                <div class="note-cols f3">
                                    <div class="item">
                                        <input type="text" class="form-control">
                                        <label for="">Subject <span>*</span></label>
                                    </div>
                                </div>
                                <div class="note-cols f4">
                                    <div class="item">
                                        <textarea name="" placeholder="Message..." id="" cols="30" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="btn-load-more">
                                <button>
                                    <span>Submit</span>
                                </button>
                            </div>
                        </form>
                    </div>
                    <div class=" col-xl-4 col-lg-5 col-md-6 col-12">
                        <div class="info">
                            <h4>Contact us</h4>
                            <div class="info-gr">
                                <div class="info-gr-item">
                                    <div class="icon">
                                        <picture>
                                            <img src="http://luxuryhair.wecan-group.info/wp-content/uploads/2023/11/Map-Point.png"
                                                 alt="">
                                        </picture>
                                    </div>
                                    <div class="item">
                                        <span>Sales Office</span>
                                        <p>27 A6, Dam Trau Quarter, Hai Ba Trung District, Hanoi, Vietnam</p>

                                    </div>
                                </div>
                                <div class="info-gr-item">
                                    <div class="icon">
                                        <picture>
                                            <img src="http://luxuryhair.wecan-group.info/wp-content/uploads/2023/11/Map-Point.png"
                                                 alt="">
                                        </picture>
                                    </div>
                                    <div class="item">
                                        <span>Contact for Travel Agent</span>
                                        <p>No 22, Port 2, Tuan Chau Port, Halong, Quang Ninh</p>
                                    </div>
                                </div>
                                <div class="info-gr-item">
                                    <div class="icon">
                                        <picture>
                                            <img src="http://luxuryhair.wecan-group.info/wp-content/uploads/2023/11/Map-Point.png"
                                                 alt="">
                                        </picture>
                                    </div>
                                    <div class="item">
                                        <span>Sales Office</span>
                                        <p>+84 86 520 16 16 | +84 98 204 24 26</p>
                                    </div>
                                </div>
                            </div>
                            <div class="contact-social">
                                <picture>
                                    <a href="">
                                        <img src="./dist/images/yt.png" alt="">
                                    </a>
                                </picture>
                                <picture>
                                    <a href="">
                                        <img src="./dist/images/tw.png" alt="">
                                    </a>
                                </picture>
                                <picture>
                                    <a href="">
                                        <img src="./dist/images/fb.png" alt="">
                                    </a>
                                </picture>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
    </main>
<?php include "footer.php"; ?>