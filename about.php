<?php include "header.php"; ?>

    <main class="main">
        <section class="about-section-1">
            <div class="banner-image">
                <picture>
                    <img src="./dist/images/8132e9e516733df9c6e9cee701bd22f2.jpg" alt="">
                </picture>
            </div>
            <div class="banner-text">
                <div class="top">
                    <h5><a href=""><img src="./dist/images/Home.png" alt=""> Home</a></h5> / <h5><span>About us</span>
                    </h5>
                </div>
                <div class="bot">
                    <h2>About us</h2>
                </div>
            </div>
        </section>
        <section class="about-section-2 section">
            <div class="container">
                <div class="content-why">
                    <div class="title">
                        <h3>Why choose <span>Indochina Sails?</span></h3>
                    </div>
                    <hr>
                    <div class="content">
                        <p>
                            Indochina Sails is a leading cruise operator working under Vietnam’s leading Huong Hai
                            Tourism Group. Luxury hospitality is the cornerstone of our mission, as we provide guests
                            with an unforgettable experience from their bedroom furnishings to staff service. Our
                            internationally recognized cruises have taken Ha Long Bay holidays to new heights, as we
                            work both for and with our customers to provide the journey of a lifetime. With a combined
                            experience of over 20 years hosting guests on the waters of Ha Long Bay, Indochina Sails has
                            developed a floating hospitality service unparalleled in most aspects. Our dedicated teams
                            know Ha Long Bay and Lan Ha Bay like the back of their hands, and so our customers get to
                            experience the best of what these world-renowned waters have to offer.
                        </p>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-section-3 section">
            <div class="container">
                <div class="div-content">
                    <div class="row dis-our align-items-center">
                        <div class="col-xl-6 col-md-7 col-sm-12">
                            <div class="content-our">
                                <div class="title">
                                    <h3>Personalized Experience</h3>
                                </div>
                                <hr>
                                <div class="content">
                                    <p>
                                        Indochina Sails is one of the first companies specializing in luxury cruises in
                                        Ha
                                        long Bay with more than 20 years of significant experience. Its 5 new ships
                                        named
                                        Indochina Sails and Valentine recently came into service in 2007, 2008 and 2010
                                        confirmed again its objectives of charm, comfort and perfect services; increased
                                        its
                                        capacity many times higher before. Indochina Sails became the first choice of
                                        many
                                        tourists from inside and outside Vietnam for their trip to the World Natural
                                        Heritage – Ha Long bay.
                                    </p>
                                    <p>
                                        From 2019 guests will be offered access to a new land experiencing its dawn in
                                        tourism. The Indochine Cruise is the newest cruise in Lan Ha Bay, a seascape
                                        full of
                                        Halong’s famous limestone spires, rugged caves and golden sands, but with far
                                        fewer
                                        tourist boats and much more in the ways of space and calm atmosphere.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-5 col-sm-12">
                            <div class="image-our">
                                <picture>
                                    <img src="./dist/images/b61e964f00c69e538d1d0682fe52d93e.jpg" alt="">
                                </picture>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="about-section-4 section">
            <div class="our-star">
                <div class="container">
                    <div class="row dis-our justify-content-between align-items-center">
                        <div class="col-xl-5 col-md-5 col-sm-12">
                            <div class="image-our">
                                <picture>
                                    <img src="./dist/images/8906b64938d02115a047dc050f51099a.jpg" alt="">
                                </picture>
                            </div>

                        </div>
                        <div class="col-xl-6 col-md-7 col-sm-12">
                            <div class="content-our">
                                <div class="title">
                                    <h3>Our Stance on Responsible Tourism</h3>
                                </div>
                                <hr>
                                <div class="content">
                                    <p>
                                        We believe, at Indochina Sails, that respect for the people and the environment
                                        in which we sail should come as standard for every tour we offer. This
                                        fundamental belief of our company has influenced our decisions over the years
                                        and has given us an ever-increasing desire to give back to the rightful owners
                                        of bay.
                                    </p>
                                    <p>
                                        To this end we introduced our responsible tourism initiative. The program of our
                                        initiative is based not only in donations, but also practical assistance and the
                                        regulations of our cruise, regular clean-up efforts
                                    </p>
                                    <p>
                                        This ties into all of our ships’ standardised policies on eco-friendly
                                        materials, which drastically reduce the amount of plastic we use in favour of
                                        glass and more natural alternatives. We also ensure that many of our other
                                        materials come from renewable sources and that wastewater is properly treated
                                        before being discharged.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>
        <section class="section about-section-5">
            <div class="container">
                <div class="person">
                    <div class="cols">
                        <div class="card ">
                            <div class="img">
                                <picture>
                                    <img src="./dist/images/ba415228b9111b5504e456219c34e715.jpg" alt="">
                                </picture>
                            </div>
                            <div class="content">
                                <p>
                                    There is a great amount of diversity and individuality between all of the Halong Bay
                                    and
                                    Lan Ha Bay cruises; yet one glance at any of our magnificent vessels will be the
                                    perfect
                                    answer to why you should cruise with Indochina Sails.
                                </p>
                                <p>
                                    Luxury stretches throughout both the Indochina Sails and the Indochine Cruise, both
                                    of
                                    which are 5-star quality and offer guests an array of facilities in which they are
                                    invited to indulge. Our luxury cruises provides extravagance through well-lit
                                    bedrooms
                                    with graceful en suites, as well as a beautiful interior of restaurant, bar and
                                    lounge.
                                    The Indochina Sails is characterised by free space and true relaxation, encapsulated
                                    best on the gorgeous sundeck, the very crest of excellence in the realms of Bay
                                    cruises.
                                    cruises
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="cols">
                        <div class="img-gallrey ">
                            <div class="img">
                                <img src="./dist/images/ac1bca600de259debf7534b1a8bf0811.jpg" alt="">
                            </div>
                            <div class="img">
                                <img src="./dist/images/d72d344aa0e61f1ae24e084fe35220bf.jpg" alt="">
                            </div>
                            <div class="img">
                                <img src="./dist/images/ac1bca600de259debf7534b1a8bf0811.jpg" alt="">
                            </div>
                            <div class="img">
                                <img src="./dist/images/d72d344aa0e61f1ae24e084fe35220bf.jpg" alt="">
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </section>
        <section class="section about-section-6">
            <div class="container">
                <div class="row justify-content-between unique">
                    <div class="col-xl-6 col-md-6 col-12">
                        <div class="img">
                            <div class="img-big">
                                <picture>
                                    <img src="./dist/images/0d5230c9aa8a2cecdc6d316db32662e8.jpg" alt="">
                                </picture>
                            </div>
                            <div class="img-mini">
                                <picture>
                                    <img src="./dist/images/0d5230c9aa8a2cecdc6d316db32662e8.jpg" alt="">
                                </picture>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-5 col-md-6 col-12">
                        <div class="content-unique">
                            <div class="title">
                                <h3>Unique Experiences</h3>
                            </div>
                            <hr>
                            <div class="content">
                                <p>
                                    Aside from our experience and luxury, Indochina Sails has famously been known for
                                    the highly individual excursions offered on our cruises. We fill our roles as
                                    innovators by offering fresh ways to absorb the majesty of Halong and Lan Ha bays.
                                </p>
                                <p>
                                    One such way in which we do this is with our ‘floating bar’, a truly magical
                                    candlelit dinner in a cave, bookable via special appointment with our staff.
                                </p>
                                <p>
                                    The Indochine Cruise is also the first cruise to offer multiple dining rooms for the
                                    freedom of our guests. Our restaurants will provide different types of food
                                    presented in different styles, meaning that passengers get to choose what they are
                                    feeling between buffet, set menu and a la carte meals.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="section about-section-7">
            <div class="container">
                <div class="services row">
                    <div class="col-12 preson">
                        <div class="box-services">
                            <div class="title">
                                <h3>Personalized Experience</h3>
                            </div>
                            <hr>
                            <div class="content">
                                <p>
                                    Aside from our experience and luxury, Indochina Sails has famously been known for
                                    the
                                    highly individual excursions offered on our cruises. We fill our roles as innovators
                                    by
                                    offering fresh ways to absorb the majesty of Halong and Lan Ha bays.
                                </p>
                                <p>
                                    One such way in which we do this is with our ‘floating bar’, a truly magical
                                    candlelit
                                    dinner in a cave, bookable via special appointment with our staff.
                                </p>
                                <p>
                                    The Indochine Cruise is also the first cruise to offer multiple dining rooms for the
                                    freedom of our guests. Our restaurants will provide different types of food
                                    presented in
                                    different styles, meaning that passengers get to choose what they are feeling
                                    between
                                    buffet, set menu and a la carte meals.
                                </p>
                            </div>
                        </div>
                        <div class="img-services">
                            <picture>
                                <img src="./dist/images/038e193e33fd4d6d0becf2fed7225fef.jpg" alt="">
                            </picture>
                        </div>
                    </div>


                </div>
            </div>
        </section>
        <section class="section about-section-8 section-last">
            <div class="container">
                <div class="title">
                    <h3>Our team</h3>
                    <p>Allow Us To Take Care Of Every Detail</p>
                </div>
                <div class="tab">
                    <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="pills-out-tab" data-bs-toggle="pill" data-bs-target="#pills-out" type="button" role="tab" aria-controls="pills-out" aria-selected="true">Out sales office</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="pills-crew-tab" data-bs-toggle="pill" data-bs-target="#pills-crew" type="button" role="tab" aria-controls="pills-crew" aria-selected="false">Out fantastic crew</button>
                        </li>
                    </ul>
                </div>
                <div class="content">
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-out" role="tabpanel" aria-labelledby="pills-out-tab">
                            <div class="img-team ">
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                            </div>
                            <div class="content-team">
                                <p>
                                    Indochina Sails takes pride in its long-lasting relationships with guests, for whom there is “No request too large, no detail too small.”
                                </p>
                                <p>
                                    We always brainstorm on what could make your itinerary happy. We always search for the less-traveled routes, wether your cruise trip is for business or pleasure, for a weekend break or a meetings & conventions, incentive & team building groups, weddings & other, we’re always delighted to welcome you.
                                </p>
                                <p>
                                    <b>Nhi Le, General Director, making owner dreams come true!</b>  She has spent his entire adult life in Vietnam hospitality industry and arguably is the most experienced CEO in the sector right now.  Highly talented and extremely motivated, the members of her management team provide a depth of industry and operating experience that is fundamental to the success of Indochina Sails
                                </p>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="pills-crew" role="tabpanel" aria-labelledby="pills-crew-tab"><div class="img-team ">
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                                <div class="item-team">
                                    <img src="./dist/images/f2dd1b1cbe90e77f39367ad104705148.jpg" alt="">
                                </div>
                            </div>
                            <div class="content-team">
                                <p>
                                    Indochina Sails takes pride in its long-lasting relationships with guests, for whom there is “No request too large, no detail too small.”
                                </p>
                                <p>
                                    We always brainstorm on what could make your itinerary happy. We always search for the less-traveled routes, wether your cruise trip is for business or pleasure, for a weekend break or a meetings & conventions, incentive & team building groups, weddings & other, we’re always delighted to welcome you.
                                </p>
                                <p>
                                    <b>Nhi Le, General Director, making owner dreams come true!</b>  She has spent his entire adult life in Vietnam hospitality industry and arguably is the most experienced CEO in the sector right now.  Highly talented and extremely motivated, the members of her management team provide a depth of industry and operating experience that is fundamental to the success of Indochina Sails
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php include "footer.php"; ?>