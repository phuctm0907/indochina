<?php include "header.php"; ?>
<main class="main">
    <section class="section breadcrumbs section-top">
        <div class="container">
            <div class="bread">
                <h5><img src="./dist/images/Home2.png" alt="">Home</h5> / <span>Gallery</span>
            </div>
        </div>
    </section>
    <section class="section-gallery">
        <div class="container">
            <div class="note-tab">
                <div class="title">
                    <h3>Indochine Cruise</h3>
                </div>
                <ul class="nav nav-pills tab-pic" id="pills-tab" role="tablist">
                    <li class="nav-item" role="presentation">
                        <button class="nav-link active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home" aria-selected="true"><img src="./dist/images/frame.png" alt="">Grid</button>
                    </li>
                    <hr>
                    <li class="nav-item" role="presentation">
                        <button class="nav-link" id="pills-profile-tab" data-bs-toggle="pill" data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile" aria-selected="false"><img src="./dist/images/row-vertical.png" alt="">Single</button>
                    </li>
                </ul>
            </div>
            <div class="tab-content" id="pills-tabContent">
                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="grid-wrapper" id="js-gallery">
                        <div class="width-2 library-image" data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>
                        <div class="library-image height-2" data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>
                        <div class="library-image" data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>
                        <div class="library-image" data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>
                        <div class="library-image height-2" data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>

                        <div class=" library-image width-2" data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>
                        <div class="library-image " data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>
                        <div class=" library-image" data-src="">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="image 1">
                            </a>
                        </div>

                    </div>
                </div>
                <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                    <div class="grid-que">
                        <div class="img-full">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="">
                            </a>
                        </div>
                        <div class="img-full">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="">
                            </a>
                        </div>
                        <div class="img-full">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="">
                            </a>
                        </div>
                        <div class="img-full">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="">
                            </a>
                        </div>
                        <div class="img-full">
                            <a href="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" data-fancybox="group">
                                <img src="http://serena.wecan-group.info/wp-content/uploads/2022/11/HVG_1262-scaled.jpg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn-load-more">
                <button>
                    <span>Learn more</span>
                </button>
            </div>
        </div>
    </section>
</main>
<?php include "footer.php"; ?>