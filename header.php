<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Indochina Sails</title>
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="author" content="" />
	<meta property="og:title" content="" />
	<meta property="og:description" content="" />
	<meta property="og:url" content="" />
	<link rel="shortcut icon" href="#" type="image/x-icon" />
	<link rel="stylesheet" type="text/css" href="dist/lib/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="dist/lib/fancybox/jquery.fancybox.min.css">
	<link rel="stylesheet" type="text/css" href="dist/lib/swiper/swiper-bundle.min.css">
	<link rel="stylesheet" type="text/css" href="dist/lib/slick/slick-theme.css">
	<link rel="stylesheet" type="text/css" href="dist/lib/slick/slick.css">
	<link rel="stylesheet" type="text/css" href="dist/assets/flickity.min.css">
	<link rel="stylesheet" type="text/css" href="dist/assets/aos.css">
	<link rel="stylesheet" type="text/css" href="dist/assets/custom.css">
	<link rel="stylesheet" href="dist/scss/gallery.css">
    <link rel="stylesheet" href="dist/scss/about.css">
    <link rel="stylesheet" href="dist/scss/cruise.css">
    <link rel="stylesheet" href="dist/scss/contact.css">
    <link rel="stylesheet" href="dist/scss/download-centent.css">
    <link rel="stylesheet" href="dist/scss/wedding.css">
</head>
<!-- <header class="header">-->
<!--	<div class="header-wrapper">-->
<!--		<div class="header-desktop">-->
<!--			<div class="logo">-->
<!--				<a href="#">-->
<!--					<figure><img src="dist/images/logo.png" alt="images"></figure>-->
<!--				</a>-->
<!--			</div>-->
<!--			<div class="mega-menu">-->
<!--				<div class="menu-wrapper">-->
<!--					<ul>-->
<!--						<li>-->
<!--							<a href="#">-->
<!--								<div class="m-links">-->
<!--									<span>Our cruises</span>-->
<!--									<i class="fas fa-sort-down"></i>-->
<!--								</div>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a href="#">-->
<!--								<div class="m-links">-->
<!--									<span>Cruise Itineraries</span>-->
<!--									<i class="fas fa-sort-down"></i>-->
<!--								</div>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a href="#">-->
<!--								<div class="m-links">-->
<!--									<span>Offers</span>-->
<!--								</div>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a href="#">-->
<!--								<div class="m-links">-->
<!--									<span>Gallery</span>-->
<!--								</div>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a href="#">-->
<!--								<div class="m-links">-->
<!--									<span>Mice & Event</span>-->
<!--								</div>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a href="#">-->
<!--								<div class="m-links">-->
<!--									<span>Wedding cruise</span>-->
<!--								</div>-->
<!--							</a>-->
<!--						</li>-->
<!--						<li>-->
<!--							<a href="#">-->
<!--								<div class="m-links">-->
<!--									<span>About us</span>-->
<!--								</div>-->
<!--							</a>-->
<!--						</li>-->
<!--					</ul>-->
<!--				</div>-->
<!--			</div>-->
<!--			<div class="header-right">-->
<!--				<div class="bnt-advand">-->
<!--					<div class="lang">-->
<!--						<button><span>English</span><i class="fas fa-sort-down"></i></button>-->
<!--					</div>-->
<!--					<div class="search">-->
<!--						<button>-->
<!--							<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 16 16" fill="none">-->
<!--								<path d="M7.66683 14.5C3.90016 14.5 0.833496 11.4333 0.833496 7.66668C0.833496 3.90001 3.90016 0.833344 7.66683 0.833344C11.4335 0.833344 14.5002 3.90001 14.5002 7.66668C14.5002 11.4333 11.4335 14.5 7.66683 14.5ZM7.66683 1.83334C4.44683 1.83334 1.8335 4.45334 1.8335 7.66668C1.8335 10.88 4.44683 13.5 7.66683 13.5C10.8868 13.5 13.5002 10.88 13.5002 7.66668C13.5002 4.45334 10.8868 1.83334 7.66683 1.83334Z" fill="#FEFEFE" />-->
<!--								<path d="M14.6668 15.1667C14.5402 15.1667 14.4135 15.12 14.3135 15.02L12.9802 13.6867C12.7868 13.4933 12.7868 13.1733 12.9802 12.98C13.1735 12.7867 13.4935 12.7867 13.6868 12.98L15.0202 14.3133C15.2135 14.5067 15.2135 14.8267 15.0202 15.02C14.9202 15.12 14.7935 15.1667 14.6668 15.1667Z" fill="#FEFEFE" />-->
<!--							</svg>-->
<!--						</button>-->
<!--						<input type="text" placeholder="Search...">-->
<!--					</div>-->
<!--				</div>-->
<!--				<button class="bttn booking">Booking your cruise</button>-->
<!--			</div>-->
<!--		</div>-->
<!--	</div>-->
<!--</header>-->