<?php include "header.php"; ?>
<main class="main">
    <section class=" breadcrumbs section-top">
        <div class="container">
            <div class="bread">
                <h5><img src="./dist/images/Home2.png" alt="">Home</h5> / <h5>Cruise Itineraries</h5>/ <h5>2 Days 1 Night</h5> / <span>Indochine Premium Ha Long</span>
            </div>
        </div>
    </section>
    <section class="section-tour">
        <div class="container">
            <div class="title">
                <h1>
                    Ha Long - 1 Night - Indochine Premium
                </h1>
            </div>
            <div class="info-booking">
                <div class="box-booking choice">
                    <div class="choice-book">
                        <div class="choice-book-item">
                            <h3>Select departure day</h3>
                            <input class="date" type="text" id="datepicker" placeholder="16 tháng 4,2023" readonly >
                        </div>
                        <div class="choice-number-item choice-number-people">
                            <h3>Amount of people</h3>
                            <div class="number">
                                <div class="number-item">
                                    <div class="type">
                                        <h4>Adult</h4>
                                        <span>(>12 age)</span>
                                    </div>
                                    <div class="select-number">
                                        <figure class="decrease">
                                            <img src="./dist/images/minus-cirlce.svg" alt="svg">
                                        </figure>
                                        <span class="main-number number-adult">01</span>
                                        <figure class="increase">
                                            <img src="./dist/images/add-circle.svg" alt="svg">
                                        </figure>
                                    </div>
                                </div>
                                <div class="number-item">
                                    <div class="type">
                                        <h4>Children</h4>
                                        <span>(> 6 age & =< 12 age)</span>
                                    </div>
                                    <div class="select-number">
                                        <figure class="decrease">
                                            <img src="./dist/images/minus-cirlce.svg" alt="svg">
                                        </figure>
                                        <span class="main-number number-children">01</span>
                                        <figure class="increase">
                                            <img src="./dist/images/add-circle.svg" alt="svg">
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="btn-load-more">
                            <button>
                                <span>Booking Now</span>
                            </button>
                        </div>
                    </div>

                </div>
                <div class="group-image">
                    <div class="big-img">
                        <picture>
                            <a href="">
                                <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                            </a>
                        </picture>
                    </div>
                    <div class="group-smail-img">
                        <div class="picture">
                            <picture>
                                <a href="">
                                    <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                                </a>
                            </picture>
                        </div>

                        <div class="picture">
                            <picture>
                                <a href="">
                                    <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                                </a>
                            </picture>
                        </div>
                        <div class="picture more">
                                <picture class="">
                                    <a href="">
                                        <img src="./dist/images/6e13f862409e51e32e39abb844862226.jpg" alt="">
                                    </a>
                                </picture>
                            <div class="img-more">
                                <a href="">
                                <span>+20</span> <img src="./dist/images/img6.svg" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<div class="show-gray"></div>
<?php include "footer.php"; ?>
<script>
    $(document).ready(function(){

        $(".date").focus(function(){
            $(".show-gray").addClass('show');
        });

        // $("#datepicker").datepicker({
        //     numberOfMonths: 2,
        //     showButtonPanel: true,
        //     dateFormat: "d 'tháng' m, yy",
        //     minDate: 0,
        //     prevText: "",
        //     nextText: "",
        //     buttonText: "",
        //     onSelect: function(dateText, inst) {
        //         var selectedDate = $(this).datepicker("getDate");
        //         var selectedMonth = selectedDate.getMonth();
        //
        //         if (selectedMonth == (new Date()).getMonth() + 1) {
        //             $(this).datepicker("setDate", "+1m");
        //         }
        //     },
        //     onClose: function(dateText, inst) {
        //         $(".show-gray").removeClass('show');
        //     }
        // });
    });

    // $(document).ready(function(){
    //     var select_route = $('.select-route button');
    //     var select_nights = $('.select-nights button');
    //
    //     select_route.click(function() {
    //         select_route.removeClass('active');
    //
    //         $(this).addClass('active');
    //     });
    //
    //     select_nights.click(function() {
    //         select_nights.removeClass('active');
    //
    //         $(this).addClass('active');
    //     });
    //
    // })

    $(document).ready(function(){
        var increase_number = $('.select-number .increase');
        var decrease_number = $('.select-number .decrease');

        increase_number.each(function() {
            $(this).on('click', function() {
                let main_number = $(this).siblings('.main-number');
                let currentNumber = parseInt(main_number.text());

                if (currentNumber > 0) {
                    if(currentNumber<9){
                        main_number.text('0' + (currentNumber + 1));
                    }else{
                        main_number.text(currentNumber + 1);
                    }
                }
            });
        })

        decrease_number.each(function() {
            $(this).on('click', function() {
                let main_number = $(this).siblings('.main-number');
                let currentNumber = parseInt(main_number.text());

                if (currentNumber > 1) {
                    if(currentNumber<=9){
                        main_number.text("0" + (currentNumber - 1));
                    }else{
                        main_number.text(currentNumber - 1);
                    }
                }
            });
        })
    })


</script>
