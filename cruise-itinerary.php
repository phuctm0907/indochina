<?php include "header.php"; ?>
<main class="main">
    <section class="our-cruise-1">
        <div class="container-fuild">
            <div class="content">
                <div class="bg-video">
                    <div class="container">
                        <div class="list-link">
                            <a href="#" class="list-link-item">
                                <figure>
                                    <img src="./dist/images/Home.svg" alt="svg">
                                </figure>
                                <span>Home</span>
                            </a>
                            <span class="untitled">/</span>
                            <a href="#" class="list-link-item">
                                <span>Cruise Itineraries</span>
                            </a>
                            <span class="untitled">/</span>
                            <a href="#" class="list-link-item active">
                                <span>2 Days 1 Night</span>
                            </a>
                            <span class="untitled">/</span>
                        </div>
                    </div>
<!--                    <video autoplay muted loop id="video">-->
<!--                        <source src="dist/images/video.mp4" type="video/mp4">-->
<!--                    </video>-->
                    <img src="./dist/images/f9b8e291ccd2542498cc92bfca92791f.jpg" alt="">
                </div>
                <div class="content-main">
                    <div class="title">
                        <h2>Cruise Itineraries</h2>
                    </div>
                    <div class="text">
                        <span>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</span>
                    </div>
                </div>
                <div class="form-search">
                    <form action="">
                        <div class="group-select">
                            <div class="dropdown">
                                <a class="select">2 Days 1 Night</a>
                                <ul class="option-list">
                                    <li><a>Apple </a></li>
                                    <li><a>Blackberry</a></li>
                                    <li><a>Cherry</a></li>
                                    <li><a>Date</a></li>
                                </ul>
                            </div>
                            <div class="dropdown">
                                <a class="select">2 Days 1 Night</a>
                                <ul class="option-list">
                                    <li><a>Apple </a></li>
                                    <li><a>Blackberry</a></li>
                                    <li><a>Cherry</a></li>
                                    <li><a>Date</a></li>
                                </ul>
                            </div>
                            <div class="dropdown">
                                <a class="select">2 Days 1 Night</a>
                                <ul class="option-list">
                                    <li><a>Apple </a></li>
                                    <li><a>Blackberry</a></li>
                                    <li><a>Cherry</a></li>
                                    <li><a>Date</a></li>
                                </ul>
                            </div>
                            <button class="search-post">
                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                    <path d="M10.5418 19.9375C5.36266 19.9375 1.146 15.7208 1.146 10.5417C1.146 5.36251 5.36266 1.14584 10.5418 1.14584C15.721 1.14584 19.9377 5.36251 19.9377 10.5417C19.9377 15.7208 15.721 19.9375 10.5418 19.9375ZM10.5418 2.52084C6.11433 2.52084 2.521 6.12334 2.521 10.5417C2.521 14.96 6.11433 18.5625 10.5418 18.5625C14.9693 18.5625 18.5627 14.96 18.5627 10.5417C18.5627 6.12334 14.9693 2.52084 10.5418 2.52084Z" fill="#2A2D2F" />
                                    <path d="M20.1668 20.8542C19.9927 20.8542 19.8185 20.79 19.681 20.6525L17.8477 18.8192C17.5818 18.5533 17.5818 18.1133 17.8477 17.8475C18.1135 17.5817 18.5535 17.5817 18.8193 17.8475L20.6527 19.6808C20.9185 19.9467 20.9185 20.3867 20.6527 20.6525C20.5152 20.79 20.341 20.8542 20.1668 20.8542Z" fill="#2A2D2F" />
                                    <path d="M10.5418 19.9375C5.36266 19.9375 1.146 15.7208 1.146 10.5417C1.146 5.36251 5.36266 1.14584 10.5418 1.14584C15.721 1.14584 19.9377 5.36251 19.9377 10.5417C19.9377 15.7208 15.721 19.9375 10.5418 19.9375ZM10.5418 2.52084C6.11433 2.52084 2.521 6.12334 2.521 10.5417C2.521 14.96 6.11433 18.5625 10.5418 18.5625C14.9693 18.5625 18.5627 14.96 18.5627 10.5417C18.5627 6.12334 14.9693 2.52084 10.5418 2.52084Z" stroke="#2A2D2F" stroke-width="0.5" />
                                    <path d="M20.1668 20.8542C19.9927 20.8542 19.8185 20.79 19.681 20.6525L17.8477 18.8192C17.5818 18.5533 17.5818 18.1133 17.8477 17.8475C18.1135 17.5817 18.5535 17.5817 18.8193 17.8475L20.6527 19.6808C20.9185 19.9467 20.9185 20.3867 20.6527 20.6525C20.5152 20.79 20.341 20.8542 20.1668 20.8542Z" stroke="#2A2D2F" stroke-width="0.5" />
                                </svg>
                                <span>Search Cruise</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="our-cruise-7">
        <div class="container">
            <div class="content">
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="content-item">
                    <a href="#">
                        <div class="picture">
                            <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                        </div>
                        <div class="desc">
                            <h3>Indochine Premium Ha Long</h3>
                            <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                            <div class="more-info">
                                <div class="price">
                                    <span class="pri-old">From $314</span>
                                    <span class="pri-sale"><strong>$275</strong>
                                            <p>/person</p>
                                        </span>
                                </div>
                                <div class="rating">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                        <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B"></path>
                                    </svg>
                                    <span>4.60 (280)</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include "footer.php";?>