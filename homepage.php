<?php include "header.php"; ?>
<main class="main">
    <section class="home-section-1">
        <div class="container-fuild">
            <div class="content">
                <div class="bg-video">
                    <div class="container">
                        <div class="text-slide">
                            <div class="home-slide-1">
                                <div class="item-slide">
                                    <div class="text">
                                        <h2>Discover<br>The Joys Of The Bay</h2>
                                        <p>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</p>
                                    </div>
                                </div>
                                <div class="item-slide">
                                    <div class="text">
                                        <h2>Discover The Joys Of The Bay</h2>
                                        <p>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</p>
                                    </div>
                                </div>
                                <div class="item-slide">
                                    <div class="text">
                                        <h2>Discover The Joys Of The Bay</h2>
                                        <p>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</p>
                                    </div>
                                </div>
                            </div>
                            <div class="counter-dots">
                                <span class="details"></span>
                                <span class="counter-status"></span>
                            </div>
                        </div>
                    </div>
                    <video autoplay muted loop id="video">
                        <source src="dist/images/video.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="advand-search">
                    <div class="container">
                        <div class="content-search">
                            <div class="box-search">
                                <div class="box">
                                    <select name="1" id="1">
                                        <option value="1">Cruise Duration</option>
                                        <option value="2">Cruise Duration</option>
                                        <option value="3">Cruise Duration</option>
                                    </select>
                                </div>
                                <div class="box">
                                    <select name="2" id="2">
                                        <option value="1">Departure</option>
                                        <option value="2">Departure</option>
                                        <option value="3">Departure</option>
                                    </select>
                                </div>
                                <div class="box">
                                    <select name="3" id="3">
                                        <option value="1">Boat Preference</option>
                                        <option value="2">Boat Preference</option>
                                        <option value="3">Boat Preference</option>
                                    </select>
                                </div>
                                <button>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" viewBox="0 0 22 22" fill="none">
                                        <path d="M10.5418 19.9375C5.36266 19.9375 1.146 15.7208 1.146 10.5417C1.146 5.36251 5.36266 1.14584 10.5418 1.14584C15.721 1.14584 19.9377 5.36251 19.9377 10.5417C19.9377 15.7208 15.721 19.9375 10.5418 19.9375ZM10.5418 2.52084C6.11433 2.52084 2.521 6.12334 2.521 10.5417C2.521 14.96 6.11433 18.5625 10.5418 18.5625C14.9693 18.5625 18.5627 14.96 18.5627 10.5417C18.5627 6.12334 14.9693 2.52084 10.5418 2.52084Z" fill="#2A2D2F" />
                                        <path d="M20.1668 20.8542C19.9927 20.8542 19.8185 20.79 19.681 20.6525L17.8477 18.8192C17.5818 18.5533 17.5818 18.1133 17.8477 17.8475C18.1135 17.5817 18.5535 17.5817 18.8193 17.8475L20.6527 19.6808C20.9185 19.9467 20.9185 20.3867 20.6527 20.6525C20.5152 20.79 20.341 20.8542 20.1668 20.8542Z" fill="#2A2D2F" />
                                        <path d="M10.5418 19.9375C5.36266 19.9375 1.146 15.7208 1.146 10.5417C1.146 5.36251 5.36266 1.14584 10.5418 1.14584C15.721 1.14584 19.9377 5.36251 19.9377 10.5417C19.9377 15.7208 15.721 19.9375 10.5418 19.9375ZM10.5418 2.52084C6.11433 2.52084 2.521 6.12334 2.521 10.5417C2.521 14.96 6.11433 18.5625 10.5418 18.5625C14.9693 18.5625 18.5627 14.96 18.5627 10.5417C18.5627 6.12334 14.9693 2.52084 10.5418 2.52084Z" stroke="#2A2D2F" stroke-width="0.5" />
                                        <path d="M20.1668 20.8542C19.9927 20.8542 19.8185 20.79 19.681 20.6525L17.8477 18.8192C17.5818 18.5533 17.5818 18.1133 17.8477 17.8475C18.1135 17.5817 18.5535 17.5817 18.8193 17.8475L20.6527 19.6808C20.9185 19.9467 20.9185 20.3867 20.6527 20.6525C20.5152 20.79 20.341 20.8542 20.1668 20.8542Z" stroke="#2A2D2F" stroke-width="0.5" />
                                    </svg>
                                    <span>Search Cruise</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
    <section class="home-section-2">
        <div class="container">
            <div class="content">
                <div class="title">
                    <h2>Vietnam’s leading cruise operator</h2>
                    <p>Art & Sustainable Journey | Always Included Luxury</p>
                </div>
                <div class="list-cruise">
                    <div class="row row-10">
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <a href="#" class="bttn loadmore">Explore all</a>
                </div>
            </div>
        </div>
    </section>
    <section class="home-section-3" style="background-image: url('dist/images/bg-1.png');">
        <div class="container">
            <div class="letter-box">
                <div class="text">
                    <h2>Overnight Cruises</h2>
                    <strong>A Great Way To Experience The Best A Destination</strong>
                    <p>One of the most spectacular New7Wonders of Nature is a UNESCO World Heritage Site - Halong Bay, translated as "descending dragon", a mystical importance to the Vietnamese people.
                        There is no better way to explore this picturesque panorama than by drifting tranquilly on the Indochina Sails's cruise, a leading cruise operator working under Vietnam’s leading Huong Hai Tourism Group.
                        With a combined experience of over 30 years hosting guests on the waters, one step aboard our nostalgia, aesthetics and you’ll see how the sunrise/ sunset are a-changin’.
                        Welcome to the luxurious colonial-style floating hotel cruising, well-regarded hospitality. Welcome aboard Indochina Sails!</p>
                    <a href="#">Booking your cruise</a>
                </div>
            </div>
        </div>
    </section>
    <section class="home-section-4">
        <div class="container">
            <div class="content">
                <div class="title">
                    <h2>Discover the joys of the bay</h2>
                    <p>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</p>
                </div>
                <div class="slide-joys">
                    <div class="home-slide-2">
                        <div class="joy-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                                        <div class="more-info">
                                            <div class="price">
                                                <span class="pri-old">From $314</span>
                                                <span class="pri-sale"><strong>$275</strong>
                                                    <p>/person</p>
                                                </span>
                                            </div>
                                            <div class="rating">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                                    <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B" />
                                                </svg>
                                                <span>4.60 (280)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="joy-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                                        <div class="more-info">
                                            <div class="price">
                                                <span class="pri-old">From $314</span>
                                                <span class="pri-sale"><strong>$275</strong>
                                                    <p>/person</p>
                                                </span>
                                            </div>
                                            <div class="rating">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                                    <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B" />
                                                </svg>
                                                <span>4.60 (280)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="joy-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                                        <div class="more-info">
                                            <div class="price">
                                                <span class="pri-old">From $314</span>
                                                <span class="pri-sale"><strong>$275</strong>
                                                    <p>/person</p>
                                                </span>
                                            </div>
                                            <div class="rating">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                                    <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B" />
                                                </svg>
                                                <span>4.60 (280)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="joy-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                                        <div class="more-info">
                                            <div class="price">
                                                <span class="pri-old">From $314</span>
                                                <span class="pri-sale"><strong>$275</strong>
                                                    <p>/person</p>
                                                </span>
                                            </div>
                                            <div class="rating">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                                    <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B" />
                                                </svg>
                                                <span>4.60 (280)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="joy-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                                        <div class="more-info">
                                            <div class="price">
                                                <span class="pri-old">From $314</span>
                                                <span class="pri-sale"><strong>$275</strong>
                                                    <p>/person</p>
                                                </span>
                                            </div>
                                            <div class="rating">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                                    <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B" />
                                                </svg>
                                                <span>4.60 (280)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="joy-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise-1.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <p class="short">Violet is named after the beautiful Violet Chirita, a delicate purple-flowered plant native to the rocky slopes...</p>
                                        <div class="more-info">
                                            <div class="price">
                                                <span class="pri-old">From $314</span>
                                                <span class="pri-sale"><strong>$275</strong>
                                                    <p>/person</p>
                                                </span>
                                            </div>
                                            <div class="rating">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="22" height="20" viewBox="0 0 22 20" fill="none">
                                                    <path d="M10.0975 0.891367C10.4597 0.132245 11.5403 0.132245 11.9025 0.891367L14.2224 5.75315C14.3682 6.05865 14.6587 6.26967 14.9943 6.31391L20.335 7.01792C21.1689 7.12784 21.5028 8.15552 20.8928 8.7346L16.9858 12.4434C16.7403 12.6764 16.6294 13.0178 16.691 13.3507L17.6719 18.6476C17.825 19.4746 16.9508 20.1098 16.2116 19.7085L11.477 17.1389C11.1795 16.9774 10.8205 16.9774 10.523 17.1389L5.78844 19.7085C5.04919 20.1098 4.175 19.4746 4.32814 18.6476L5.30897 13.3507C5.3706 13.0178 5.25966 12.6764 5.01416 12.4434L1.10723 8.7346C0.497195 8.15552 0.831107 7.12784 1.66501 7.01792L7.00573 6.31391C7.34134 6.26967 7.63178 6.05865 7.77756 5.75315L10.0975 0.891367Z" fill="#EEBA2B" />
                                                </svg>
                                                <span>4.60 (280)</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home-section-5">
        <div class="container">
            <div class="content">
                <div class="sub-title">
                    <h2>MICE & Event</h2>
                    <a href="#"><span>Explore All</span><i class="fal fa-arrow-right"></i></a>
                </div>
                <div class="list-event">
                    <div class="row row-10">
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-3 p-10 col-item">
                            <div class="box">
                                <a href="#">
                                    <div class="picture">
                                        <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                    </div>
                                    <div class="desc">
                                        <h3>Indochine Premium Ha Long</h3>
                                        <span class="line"></span>
                                        <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home-section-6">
        <div class="container" style="position: relative; z-index: 2;">
            <div class="content">
                <div class="title">
                    <h2>Did you know ?</h2>
                </div>
                <div class="list-know">
                    <div class="row row-10">
                        <div class="col-md-6 col-lg-4 p-10 col-item">
                            <div class="box">
                                <div class="picture">
                                    <figure><img src="dist/images/know.png" alt="images"></figure>
                                </div>
                                <div class="desc">
                                    <h3>Vietnam's leading cruise operator</h3>
                                    <p>In the 1990s, Indochina Sails pioneered Halong Bay tourism, introducing the first overnight cruises that shaped our legacy. We promise a consistently luxurious and unforgettable experience. Our cruises have elevated Ha Long Bay & Lan Ha Bay holidays, firmly cementing our reputable position as a trusted regional operator.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 p-10 col-item">
                            <div class="box">
                                <div class="picture">
                                    <figure><img src="dist/images/know-1.png" alt="images"></figure>
                                </div>
                                <div class="desc">
                                    <h3>Vietnam's leading cruise operator</h3>
                                    <p>In the 1990s, Indochina Sails pioneered Halong Bay tourism, introducing the first overnight cruises that shaped our legacy. We promise a consistently luxurious and unforgettable experience. Our cruises have elevated Ha Long Bay & Lan Ha Bay holidays, firmly cementing our reputable position as a trusted regional operator.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-4 p-10 col-item">
                            <div class="box">
                                <div class="picture">
                                    <figure><img src="dist/images/know-2.png" alt="images"></figure>
                                </div>
                                <div class="desc">
                                    <h3>Vietnam's leading cruise operator</h3>
                                    <p>In the 1990s, Indochina Sails pioneered Halong Bay tourism, introducing the first overnight cruises that shaped our legacy. We promise a consistently luxurious and unforgettable experience. Our cruises have elevated Ha Long Bay & Lan Ha Bay holidays, firmly cementing our reputable position as a trusted regional operator.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home-section-7">
        <div class="container">
            <div class="content">
                <div class="list-gallery">
                    <div class="type-list">
                        <h2>Gallery</h2>
                        <div class="title-type">
                            <ul class="type" id="type-tab">
                                <li rel="#type1" class="type-item">
                                    <button class="type-link"><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M6.89199 18.9582H3.52533C1.85033 18.9582 1.04199 18.1832 1.04199 16.5832V3.4165C1.04199 1.8165 1.85866 1.0415 3.52533 1.0415H6.89199C8.56699 1.0415 9.37533 1.8165 9.37533 3.4165V16.5832C9.37533 18.1832 8.55866 18.9582 6.89199 18.9582ZM3.52533 2.2915C2.46699 2.2915 2.29199 2.57484 2.29199 3.4165V16.5832C2.29199 17.4248 2.46699 17.7082 3.52533 17.7082H6.89199C7.95033 17.7082 8.12533 17.4248 8.12533 16.5832V3.4165C8.12533 2.57484 7.95033 2.2915 6.89199 2.2915H3.52533Z" fill="#2A2D2F" />
                                            <path d="M16.475 9.37484H13.1083C11.4333 9.37484 10.625 8.63317 10.625 7.09984V3.3165C10.625 1.78317 11.4417 1.0415 13.1083 1.0415H16.475C18.15 1.0415 18.9583 1.78317 18.9583 3.3165V7.0915C18.9583 8.63317 18.1417 9.37484 16.475 9.37484ZM13.1083 2.2915C11.9917 2.2915 11.875 2.60817 11.875 3.3165V7.0915C11.875 7.80817 11.9917 8.1165 13.1083 8.1165H16.475C17.5917 8.1165 17.7083 7.79984 17.7083 7.0915V3.3165C17.7083 2.59984 17.5917 2.2915 16.475 2.2915H13.1083Z" fill="#2A2D2F" />
                                            <path d="M16.475 18.9583H13.1083C11.4333 18.9583 10.625 18.1417 10.625 16.475V13.1083C10.625 11.4333 11.4417 10.625 13.1083 10.625H16.475C18.15 10.625 18.9583 11.4417 18.9583 13.1083V16.475C18.9583 18.1417 18.1417 18.9583 16.475 18.9583ZM13.1083 11.875C12.125 11.875 11.875 12.125 11.875 13.1083V16.475C11.875 17.4583 12.125 17.7083 13.1083 17.7083H16.475C17.4583 17.7083 17.7083 17.4583 17.7083 16.475V13.1083C17.7083 12.125 17.4583 11.875 16.475 11.875H13.1083Z" fill="#2A2D2F" />
                                        </svg><span>Grid</span>
                                    </button>
                                </li>
                                <li rel="#type2" class="type-item">
                                    <button class="type-link"><svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M16.5837 18.9583H3.41699C1.81699 18.9583 1.04199 18.1417 1.04199 16.475V13.1083C1.04199 11.4333 1.81699 10.625 3.41699 10.625H16.5837C18.1837 10.625 18.9587 11.4417 18.9587 13.1083V16.475C18.9587 18.1417 18.1837 18.9583 16.5837 18.9583ZM3.41699 11.875C2.57533 11.875 2.29199 12.05 2.29199 13.1083V16.475C2.29199 17.5333 2.57533 17.7083 3.41699 17.7083H16.5837C17.4253 17.7083 17.7087 17.5333 17.7087 16.475V13.1083C17.7087 12.05 17.4253 11.875 16.5837 11.875H3.41699Z" fill="#2A2D2F" />
                                            <path d="M16.5837 9.37484H3.41699C1.81699 9.37484 1.04199 8.55817 1.04199 6.8915V3.52484C1.04199 1.84984 1.81699 1.0415 3.41699 1.0415H16.5837C18.1837 1.0415 18.9587 1.85817 18.9587 3.52484V6.8915C18.9587 8.55817 18.1837 9.37484 16.5837 9.37484ZM3.41699 2.2915C2.57533 2.2915 2.29199 2.4665 2.29199 3.52484V6.8915C2.29199 7.94984 2.57533 8.12484 3.41699 8.12484H16.5837C17.4253 8.12484 17.7087 7.94984 17.7087 6.8915V3.52484C17.7087 2.4665 17.4253 2.2915 16.5837 2.2915H3.41699Z" fill="#2A2D2F" />
                                        </svg><span>Single</span>
                                    </button>
                                </li>
                            </ul>
                            <a href="#" class="readmore">
                                <span>Explore All</span><svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7002 5.29023L8.70023 0.290234C8.52023 0.100234 8.27023 -0.00976562 7.99023 -0.00976562C7.44023 -0.00976562 6.99023 0.440234 6.99023 0.990234C6.99023 1.27023 7.10023 1.52023 7.28023 1.70023L10.5702 4.99023H0.990234C0.440234 4.99023 -0.00976562 5.44023 -0.00976562 5.99023C-0.00976562 6.54023 0.440234 6.99023 0.990234 6.99023H10.5802L7.29023 10.2802C7.11023 10.4602 7.00023 10.7102 7.00023 10.9902C7.00023 11.5402 7.45023 11.9902 8.00023 11.9902C8.28023 11.9902 8.53023 11.8802 8.71023 11.7002L13.7102 6.70023C13.8902 6.52023 14.0002 6.27023 14.0002 5.99023C14.0002 5.71023 13.8802 5.47023 13.7002 5.29023Z" fill="#386641" />
                                </svg>
                            </a>
                        </div>
                    </div>
                    <div class="type-content">
                        <div id="type1" class="tab-pane">
                            <div class="list-images">
                                <div class="row row-10">
                                    <div class="col-lg-6 col-left p-10">
                                        <div class="box">
                                            <a href="#">
                                                <div class="picture">
                                                    <figure><img src="dist/images/box-1.png" alt="image"></figure>
                                                </div>
                                                <div class="desc">
                                                    <h3>Valentine Ha Long</h3>
                                                    <button>
                                                        <span>Explore Al</span><svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7002 5.29023L8.70023 0.290234C8.52023 0.100234 8.27023 -0.00976562 7.99023 -0.00976562C7.44023 -0.00976562 6.99023 0.440234 6.99023 0.990234C6.99023 1.27023 7.10023 1.52023 7.28023 1.70023L10.5702 4.99023H0.990234C0.440234 4.99023 -0.00976562 5.44023 -0.00976562 5.99023C-0.00976562 6.54023 0.440234 6.99023 0.990234 6.99023H10.5802L7.29023 10.2802C7.11023 10.4602 7.00023 10.7102 7.00023 10.9902C7.00023 11.5402 7.45023 11.9902 8.00023 11.9902C8.28023 11.9902 8.53023 11.8802 8.71023 11.7002L13.7102 6.70023C13.8902 6.52023 14.0002 6.27023 14.0002 5.99023C14.0002 5.71023 13.8802 5.47023 13.7002 5.29023Z" fill="#FEFEFE" />
                                                        </svg>
                                                    </button>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-lg-6 col-right p-10">
                                        <div class="big-item">
                                            <div class="box">
                                                <a href="#">
                                                    <div class="picture">
                                                        <figure><img src="dist/images/box-1.png" alt="image"></figure>
                                                    </div>
                                                    <div class="desc">
                                                        <h3>Valentine Ha Long</h3>
                                                        <button>
                                                            <span>Explore Al</span><svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7002 5.29023L8.70023 0.290234C8.52023 0.100234 8.27023 -0.00976562 7.99023 -0.00976562C7.44023 -0.00976562 6.99023 0.440234 6.99023 0.990234C6.99023 1.27023 7.10023 1.52023 7.28023 1.70023L10.5702 4.99023H0.990234C0.440234 4.99023 -0.00976562 5.44023 -0.00976562 5.99023C-0.00976562 6.54023 0.440234 6.99023 0.990234 6.99023H10.5802L7.29023 10.2802C7.11023 10.4602 7.00023 10.7102 7.00023 10.9902C7.00023 11.5402 7.45023 11.9902 8.00023 11.9902C8.28023 11.9902 8.53023 11.8802 8.71023 11.7002L13.7102 6.70023C13.8902 6.52023 14.0002 6.27023 14.0002 5.99023C14.0002 5.71023 13.8802 5.47023 13.7002 5.29023Z" fill="#FEFEFE" />
                                                            </svg>
                                                        </button>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="small-item">
                                            <div class="row row-10">
                                                <div class="col-md-6 p-10 col-item">
                                                    <div class="box">
                                                        <a href="#">
                                                            <div class="picture">
                                                                <figure><img src="dist/images/box-1.png" alt="image"></figure>
                                                            </div>
                                                            <div class="desc">
                                                                <h3>Valentine Ha Long</h3>
                                                                <button>
                                                                    <span>Explore Al</span><svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7002 5.29023L8.70023 0.290234C8.52023 0.100234 8.27023 -0.00976562 7.99023 -0.00976562C7.44023 -0.00976562 6.99023 0.440234 6.99023 0.990234C6.99023 1.27023 7.10023 1.52023 7.28023 1.70023L10.5702 4.99023H0.990234C0.440234 4.99023 -0.00976562 5.44023 -0.00976562 5.99023C-0.00976562 6.54023 0.440234 6.99023 0.990234 6.99023H10.5802L7.29023 10.2802C7.11023 10.4602 7.00023 10.7102 7.00023 10.9902C7.00023 11.5402 7.45023 11.9902 8.00023 11.9902C8.28023 11.9902 8.53023 11.8802 8.71023 11.7002L13.7102 6.70023C13.8902 6.52023 14.0002 6.27023 14.0002 5.99023C14.0002 5.71023 13.8802 5.47023 13.7002 5.29023Z" fill="#FEFEFE" />
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 p-10 col-item">
                                                    <div class="box">
                                                        <a href="#">
                                                            <div class="picture">
                                                                <figure><img src="dist/images/box-1.png" alt="image"></figure>
                                                            </div>
                                                            <div class="desc">
                                                                <h3>Valentine Ha Long</h3>
                                                                <button>
                                                                    <span>Explore Al</span><svg width="14" height="12" viewBox="0 0 14 12" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M13.7002 5.29023L8.70023 0.290234C8.52023 0.100234 8.27023 -0.00976562 7.99023 -0.00976562C7.44023 -0.00976562 6.99023 0.440234 6.99023 0.990234C6.99023 1.27023 7.10023 1.52023 7.28023 1.70023L10.5702 4.99023H0.990234C0.440234 4.99023 -0.00976562 5.44023 -0.00976562 5.99023C-0.00976562 6.54023 0.440234 6.99023 0.990234 6.99023H10.5802L7.29023 10.2802C7.11023 10.4602 7.00023 10.7102 7.00023 10.9902C7.00023 11.5402 7.45023 11.9902 8.00023 11.9902C8.28023 11.9902 8.53023 11.8802 8.71023 11.7002L13.7102 6.70023C13.8902 6.52023 14.0002 6.27023 14.0002 5.99023C14.0002 5.71023 13.8802 5.47023 13.7002 5.29023Z" fill="#FEFEFE" />
                                                                    </svg>
                                                                </button>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="type2" class="tab-pane "></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="home-section-8" style="background-image: url('dist/images/bg-2.png');">
        <div class="container">
            <div class="content">
                <div class="text">
                    <h2>Get up to 30% discount for booking your first trip</h2>
                    <a href="#">Booking your cruise</a>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include "footer.php"; ?>