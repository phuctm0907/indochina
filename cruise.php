<?php include "header.php"; ?>
<main class="main">
    <section class="our-cruise-1">
        <div class="container-fuild">
            <div class="content">
                <div class="bg-video">
                    <div class="container">
                        <div class="list-link">
                            <a href="#" class="list-link-item">
                                <figure>
                                    <img src="./dist/images/Home.svg" alt="svg">
                                </figure>
                                <span>Home</span>
                            </a>
                            <span class="untitled">/</span>
                            <a href="#" class="list-link-item">
                                <span>Cruise Itineraries</span>
                            </a>
                            <span class="untitled">/</span>
                            <a href="#" class="list-link-item active">
                                <span>2 Days 1 Night</span>
                            </a>
                            <span class="untitled">/</span>
                        </div>
                    </div>
                    <video autoplay muted loop id="video">
                        <source src="dist/images/video.mp4" type="video/mp4">
                    </video>
                </div>
                <div class="content-main">
                    <div class="title">
                        <h2>Cruise Itineraries</h2>
                    </div>
                    <div class="text">
                        <span>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</span>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="cruise-section-1 section section-last">
        <div class="container">
            <div class="title">
                <h3>Attractive location</h3>
                <hr>
            </div>
            <div class="list-event">
                <div class="row row-10">
                    <div class="col-md-6 col-lg-4 col-12 p-10 col-item">
                        <div class="box">
                            <a href="#">
                                <div class="picture">
                                    <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                </div>
                                <div class="desc">
                                    <h3>Indochine Premium Ha Long</h3>
                                    <span class="line"></span>
                                    <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 col-12 p-10 col-item">
                        <div class="box">
                            <a href="#">
                                <div class="picture">
                                    <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                </div>
                                <div class="desc">
                                    <h3>Indochine Premium Ha Long</h3>
                                    <span class="line"></span>
                                    <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-4 p-10 col-item">
                        <div class="box">
                            <a href="#">
                                <div class="picture">
                                    <figure><img src="dist/images/cruise.png" alt="images"></figure>
                                </div>
                                <div class="desc">
                                    <h3>Indochine Premium Ha Long</h3>
                                    <span class="line"></span>
                                    <p>Launching in August 2023, Indochine Premium is the most newest - luxurious - distinct aesthetic overnight cruises in Ha Long Bay, Vietnam. Bringing European features together with an Asian essence with 37 luxury...</p>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php include "footer.php"; ?>