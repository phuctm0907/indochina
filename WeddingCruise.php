<?php include "header.php"; ?>
    <main class="main">
        <section class="our-cruise-1">
            <div class="container-fuild">
                <div class="content">
                    <div class="bg-video">
                        <div class="container">
                            <div class="list-link">
                                <a href="#" class="list-link-item">
                                    <figure>
                                        <img src="./dist/images/Home.svg" alt="svg">
                                    </figure>
                                    <span>Home</span>
                                </a>
                                <span class="untitled">/</span>
                                <a href="#" class="list-link-item active">
                                    <span>2 Days 1 Night</span>
                                </a>
                                <span class="untitled">/</span>
                            </div>
                        </div>
                        <!--                    <video autoplay muted loop id="video">-->
                        <!--                        <source src="dist/images/video.mp4" type="video/mp4">-->
                        <!--                    </video>-->
                        <img src="./dist/images/f9b8e291ccd2542498cc92bfca92791f.jpg" alt="">
                    </div>
                    <div class="content-main">
                        <div class="title">
                            <h2>Cruise Itineraries</h2>
                        </div>
                        <div class="text">
                            <span>Experiences for all generations. Start your day by saluting the sun with taichi on the private sundeck and relaxing over a leisurely breakfast before heading out into the sparkling bay</span>
                        </div>
                    </div>

                </div>
            </div>
        </section>
        <section class="about-section-3 wedding-crui section">
            <div class="container">
                <div class="div-content">
                    <div class="row dis-our align-items-center">
                        <div class="col-xl-5 col-md-5 col-sm-12">
                            <div class="image-our">
                                <picture>
                                    <img src="./dist/images/b61e964f00c69e538d1d0682fe52d93e.jpg" alt="">
                                </picture>
                            </div>

                        </div>
                        <div class="col-xl-6 col-md-7 col-sm-12">
                            <div class="content-our">
                                <div class="title">
                                    <h3>Rental the whole cruise</h3>
                                    <p>Enjoy the specialty of senting an entire cruise</p>
                                </div>
                                <hr>
                                <div class="content">
                                    <p>
                                        If you seek a distinctive offshore event venue for personalized parties,
                                        launches, appreciations, or elegant weddings, Indochina Sails - Indochine Cruise
                                        in Ha Long Bay excels.
                                    </p>
                                    <p>
                                        With 30+ years of serving clients globally, they are renowned for professional
                                        event planning. The team, ambitious and passionate, aims to help you create an
                                        unforgettable event!
                                    </p>
                                </div>
                                <div class="gr-button">
                                    <div class="dowload">
                                        <button href="">
                                            <span>Download MICE Brochure</span></button>
                                    </div>
                                    <div class="contactNow">
                                        <button href=""><span>Contact us now</span></button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <section class="section weddingcrui-3">
            <div class="bg-yle section section-last">
                <div class="container">
                    <div class="row pice">
                        <div class="col-xl-6 col-md-6 col-12">
                            <div class="form-contact">
                                <div class="title">
                                    <h2>
                                        Price quote for renting the whole ship
                                    </h2>
                                    <p>Please provide information to receive the most reasonable consultation & quote for your group.</p>
                                </div>
                                <form action="">
                                    <div class="gr-note">
                                        <div class="note-cols f1">
                                            <div class="item">
                                                <input type="text" class="form-control" >
                                                <label for="">Name <span>*</span></label>
                                            </div>

                                        </div>
                                        <div class="note-cols f1">
                                            <div class="item">
                                                <input type="email" class="form-control">
                                                <label for="">Email <span>*</span></label>
                                            </div>

                                        </div>
                                        <div class="note-cols f2">
                                            <div class="item phone">
                                                <select name="" id="">
                                                    <option value="">+84</option>
                                                    <option value="">+83</option>
                                                    <option value="">+123</option>
                                                    <option value="">+77</option>
                                                </select>
                                                <input type="tel" class="form-control" placeholder="Mobile Phone">
                                            </div>
                                        </div>
                                        <div class="note-cols f4">
                                            <div class="item">
                                                <textarea name="" placeholder="Message..." id="" cols="30" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="btn-load-more">
                                        <button>
                                            <span>Submit</span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="col-xl-5 col-md-6 col-12">
                            <div class="img">
                                <picture>
                                    <img src="./dist/images/8906b64938d02115a047dc050f51099a.jpg" alt="">
                                </picture>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>
<?php include "footer.php"; ?>